#!/usr/bin/env bash

# Copy static site
#CWD=`/home/devil/dockerizing-django`

# Clone Git repository
cd /home/devil/dockerizing-django
#git clone git@bitbucket.org:python_dev_lab/dockerizing-django.git
git pull

# Trigger Docker Build
docker-compose build web
docker-compose build nginx

# Trigger Docker Restart the Service
docker-compose up --no-deps -d web
docker-compose up --no-deps -d nginx

# Run Django Migration if Available
docker-compose run web /usr/local/bin/python manage.py migrate

# Stop and Remove the Django Migration Docker
docker stop dockerizingdjango_web_run_2
docker rm dockerizingdjango_web_run_2

# docker cleanup => remove and delete unused containers and images
#docker rm `docker ps -aq --no-trunc --filter "status=exited"`
#docker rmi `docker images --filter 'dangling=true' -q --no-trunc`