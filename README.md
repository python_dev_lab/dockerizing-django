## Django Development With Docker Compose and Machine

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)]() [![PyPI](https://img.shields.io/pypi/pyversions/Django.svg)]() [![Requirements Status](https://requires.io/bitbucket/python_dev_lab/dockerizing-django/requirements.svg?branch=master)](https://requires.io/bitbucket/python_dev_lab/dockerizing-django/requirements/?branch=master) [![Build Status](https://circleci.com/bb/python_dev_lab/dockerizing-django.svg?branch=master)](https://circleci.com/bb/python_dev_lab/dockerizing-django) [![Codacy Badge](https://api.codacy.com/project/badge/Grade/2e079736da9c451aaa83589e91cabd2d)](https://www.codacy.com/app/nareshskyrocket/dockerizing-django?utm_source=python_dev_lab@bitbucket.org&amp;utm_medium=referral&amp;utm_content=python_dev_lab/dockerizing-django&amp;utm_campaign=Badge_Grade)

Featuring:

- Docker v1.10.3
- Docker Compose v1.6.2
- Docker Machine v0.6.0
- Python 3.5

Blog post -> https://realpython.com/blog/python/django-development-with-docker-compose-and-machine/

### OS X Instructions

1. Start new machine - `docker-machine create -d virtualbox dev;`
1. Build images - `docker-compose build`
1. Start services - `docker-compose up -d`
1. Create migrations - `docker-compose run web /usr/local/bin/python manage.py migrate`
1. Grab IP - `docker-machine ip dev` - and view in your browser
